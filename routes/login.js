const INVALID_LOG_PASS = require('../validator/type').INVALID_LOG_PASS
const router = require('express').Router()
const User = require('../models/User')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const validateLoginInputs = require('../validator/login')



router.post('/', async (req, res) => {
    const {errors, isValid} = validateLoginInputs(req.body)

    if(!isValid) {
        return res.json({errors})
    }

    const user = await User.findOne({email: req.body.email})
    if(!user){
        errors.status = "error"
        errors.message = INVALID_LOG_PASS
        return res.json({errors})
    } 

   try{
    if(await bcrypt.compare(req.body.password, user.password)) {

        const token = jwt.sign({_id: user. _id}, process.env.TOKEN_SECRET)
        return res.json({"status": "success", token: `bearer ${token}`})
        
    }
    errors.status = "error"
    errors.message = INVALID_LOG_PASS
    return res.json({errors})
    
   } catch(error) {
       res.json(error)
   }
})




module.exports = router