const router = require('express').Router()
const jwt = require('jsonwebtoken')
const passport = require('passport')

router.get('/facebook', passport.authenticate('facebook', {scope: 'email'}));

router.get('/facebook/callback',
  passport.authenticate('facebook', {
    failureRedirect: '/login',
    session: false }),
    (req, res) => {
    const token = jwt.sign({_id: req.user. _id}, process.env.TOKEN_SECRET)
                                
    res.send(`<script>
    localStorage.setItem("token", "bearer ${token}")
    location.href = "/profile"
    </script>`)
  }
)

router.get('/google', passport.authenticate('google', {scope: ['email', 'profile']}));

router.get('/google/callback',
  passport.authenticate('google', {
    failureRedirect: '/login',
    session: false 
  }), 
  (req, res) => {
    const token = jwt.sign({_id: req.user._id}, process.env.TOKEN_SECRET)
                                
    res.send(`<script>
    localStorage.setItem("token", "bearer ${token}")
    location.href = "/profile"
    </script>`)
    }
)

router.get('/linkedin',
  passport.authenticate('linkedin',{scope: ["r_liteprofile", "r_emailaddress"]})
);

router.get('/linkedin/callback',
  passport.authenticate('linkedin', {
    failureRedirect: '/login',
    session: false
  }), 
  async (req, res) => {
            
    const token = jwt.sign({_id: req.user._id}, process.env.TOKEN_SECRET)
                                
    res.send(`<script>
    localStorage.setItem("token", "bearer ${token}")
    location.href = "/profile"
    </script>`)
    }
)
module.exports = router
