const { REGISTERED, MAIL_EXIST } = require('../validator/type');

const router = require('express').Router()
const User = require('../models/User')
const bcrypt = require('bcrypt')
const regMail = require('../emails/registration')
const transporter = require('../config/sendMail')
const validateRegisterInputs = require('../validator/register')



router.post('/', async (req, res) => {
    const{errors, isValid} = validateRegisterInputs(req.body)
    
    if(!isValid) {
        return res.json({errors});
    }

    const emailExist = await User.findOne({email: req.body.email})
    if(emailExist) {
        errors.status = "error"
        errors.message = MAIL_EXIST
        return res.send({errors})
    } 

    const{email, password} = req.body
    const hashedPassword = await bcrypt.hash(password, 10)

    const user = await new User({
        email,
        password: hashedPassword
    })
    try{
        
        await user.save()
        await transporter.sendMail(regMail(email))
        return res.send({status: "success", message: REGISTERED})
    } catch(error) {
        res.status(401).json({status:"error", message: "Щось пішло не так. Повторіть пізніше"})
    }   
    
})





module.exports = router