const {Schema, model} = require('mongoose')

const userSchema = new Schema({
    email:{
        type: String,
        require: true,
        min: 6
    },

    password: {
        type: String,
        require: true,
        min: 6
    },

    data: {
        type: Date,
        default: Date.now
    },

    resetToken: String,
    resetTokenExp: Date,
    
    facebook: {
        id: String,
        displayName: String,
        email: String,
        token: String
    },

    google: {
        id: String,
        displayName: String,
        email: String,
        token: String
    },

    linkedin: {
        id: String,
        displayName: String,
        email: String,
        token: String
    }
})

module.exports = model('User', userSchema)