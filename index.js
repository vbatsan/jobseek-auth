const express = require('express')
const mongoose = require('mongoose')
const app = express()
const dotenv = require('dotenv')
const register = require('./routes/register')
const login = require('./routes/login')
const bodyParser = require('body-parser')
const jwtStategy = require('./config/passport').jwtStrategy
const facebookStrategy =  require('./config/passport').facebookStrategy
const googleStrategy = require('./config/passport').googleStrategy
const linkedinStrategy = require('./config/passport').linkedinStrategy
const passport = require('passport')
const mailConfirm = require('./routes/mailConfirm')
const resPass = require('./routes/resetpassword')
const setPass = require('./routes/setNewPass')
const logOut = require('./routes/logout')
const cors = require('cors')
const path = require('path')
const socialAuth = require('./routes/socialAuth')
dotenv.config()


// Database
mongoose.connect(process.env.DATA_BASE, 
    {   useNewUrlParser: true,
        useUnifiedTopology: true 
    }, () => console.log("connected to db")
)

//Midllewares
app.use(cors())
app.use(bodyParser.urlencoded({extended: true}))  
app.use(bodyParser.json())

//Passport-Strategies
passport.use(jwtStategy)
passport.use(facebookStrategy)
passport.use(googleStrategy)
passport.use(linkedinStrategy)

app.use(passport.initialize())


// Routes
app.use('/register', register)
app.use('/login', login)
app.use('/logout', logOut)
app.use('/mailconfirm', mailConfirm)
app.use('/resetpassword', resPass)
app.use('/setpassword', setPass)
app.use('/api/auth', socialAuth)

if(process.env.NODE_ENV === "production") {
  app.use(express.static('client/build'))

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"))
  })
}

const PORT = process.env.PORT || 5000
app.listen(PORT, () => console.log(`server started on ${PORT}`))
